import traceback
import redis


def WorkCheck():
    try:

        # HERE SOME INITIAL WORK IS DONE THAT SCRIPTS 1 & 2 NEED TO WAIT FOR
        # IDs SERIAL PORTS
        # SAVE TO db

        r = redis.StrictRedis(host='77.244.213.126', port=10010)                          # Connect to local Redis instance

        p = r.pubsub()                                                    # See https://github.com/andymccurdy/redis-py/#publish--subscribe

        print("Starting main scripts...")

        r.publish('startScripts', 'START')                                # PUBLISH START message on startScripts channel

        print("Done")

    except Exception as e:
        print("!!!!!!!!!! EXCEPTION !!!!!!!!!")
        print(str(e))
        print(traceback.format_exc())



if __name__=="__main__":
    WorkCheck()