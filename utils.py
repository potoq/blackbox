import random
import urllib
from datetime import datetime, timedelta
from urllib.parse import urlencode

import requests

#  {'address': 'город Москва, улица Митинская, дом 36',
#  'dist': 7.243083507577732,
#  'lat': 37.363376,
#  'lon': 55.846118,
#  'path_link': 'https://www.google.com/maps/dir/55.778961,37.400621/55.846118,37.363376',
#  'visit_time': '3:23'}

def add_diff_time(branches):
    dt_now = datetime.now()
    for branch in branches:
        parse = datetime.strptime(branch['visit_time'], '%H:%M')
        branch_time = datetime(dt_now.year, dt_now.month, dt_now.day, parse.hour, parse.minute)
        total_minutes = (branch_time - dt_now).total_seconds() / 60
        if total_minutes < 0:
            branch_time = branch_time + timedelta(days=1)
            total_minutes = (branch_time - dt_now).total_seconds() / 60
        diff_time = int(total_minutes)
        branch['diff_time'] = diff_time


def convert_time_to_date(time):
    dt_now = datetime.now()
    parse = datetime.strptime(time, '%H:%M')
    branch_time = datetime(dt_now.year, dt_now.month, dt_now.day, parse.hour, parse.minute)
    total_minutes = (branch_time - dt_now).total_seconds() / 60
    if total_minutes < 0:
        branch_time = branch_time + timedelta(days=1)
    return branch_time

def add_tiket_info(branch):
    branch['ticket'] = int(1000000 * random.random() + 1)
    branch['window'] = 'M' + str(int(10 * random.random() + 1))


def get_tz(dt):
    moscow = 3
    tz = dt.hour - moscow
    h = str(tz) if tz > 9 else ('0' + str(tz))
    m = str(dt.minute) if dt.minute > 9 else ('0' + str(dt.minute))
    return h + m + '00'

# https://calendar.google.com/calendar/r/eventedit?
# text=My+Custom+Event&
# dates=20180512T230000Z/20180513T030000Z&
# details=For+details,+link+here:+https://example.com/tickets-43251101208&
# location=Garage+Boston+-+20+Linden+Street+-+Allston,+MA+02134
def add_calendar_info(branch):
    dt_visit_start = convert_time_to_date(branch['visit_time'])
    dt_visit_end = dt_visit_start + timedelta(minutes=15)
    link = 'https://calendar.google.com/calendar/r/eventedit?'
    f = {'text' : 'Запись в банк УРАЛСИБ',
         'dates': dt_visit_start.strftime('%Y%m%dT') + get_tz(dt_visit_start) + 'Z/' +
                  dt_visit_end.strftime('%Y%m%dT') + get_tz(dt_visit_end) + 'Z',
          #'details': '',
         'location': branch['address']}
    branch['google_calendar_link'] = link + urlencode(f)


def add_points(user_id, coin):
    url = 'http://77.244.213.126:10010'

    current_coin = 0
    try:
        user = requests.get(url + '/v1/u/' + str(user_id) + '/coin').json()
        if user['coin']:
            current_coin = int(user['coin'])
    except Exception as e:
        print(e)

    current_coin += coin
    a = {
        'coin': current_coin
    }
    response = requests.post(url + '/v1/u/' + str(user_id) + '/coin', json=a)


if __name__ == "__main__":
    add_points('6666', 1)