import json

from flask import Flask, jsonify, request

from blackbox import BlackBox
from categories import category, service
from parse_xml import get_branches
from utils import add_diff_time, add_tiket_info, add_calendar_info, add_points

app = Flask(__name__)
blackbox = BlackBox()


@app.route('/fetch', methods=['POST'])
def fetch():
    print(request)
    data = request.get_json()
    print(data)
    user_id = data['user_id']
    service_id = data['service_id']
    lat, lon = data['lat'], data['lon']
    if 'transport' in data:
        transport = data['transport']
    suggestions = blackbox.nearest_branches(user_id, service_id, (lat, lon))
    add_diff_time(suggestions)
    print(json.dumps(suggestions, indent=2))
    return jsonify(suggestions)


@app.route('/choose', methods=['POST'])
def choose():
    data = request.json
    user_id = data['user_id']
    branch_id = int(data['branch_id'])
    result = blackbox.info_about_chosen_branch(user_id, branch_id)
    add_tiket_info(result)
    add_calendar_info(result)
    add_points(user_id, 1)
    return jsonify(result)


@app.route('/services', methods=['GET'])
def services():
    branches = get_branches()
    result = dict()
    for b in branches:
        for c in b.categories.keys():
            result[c] = category[c]
    print(json.dumps(result, indent=2))
    return jsonify(result)


@app.route('/test', methods=['GET'])
def test():
    result = {
    "update_id": 139983033,
    "message": {
        "message_id": 145,
        "from": {
            "id": 245055029,
            "is_bot": False,
            "first_name": "Name111",
            "username": "username111",
            "language_code": "ru"
        },
        "chat": {
            "id": 245055029,
            "first_name": "Name111",
            "username": "username111",
            "type": "private"
        },
        "date": 1556441691,
        "location": {
            "latitude": 55.815483,
            "longitude": 37.513376
        }
    }
}
    return jsonify(result)


@app.route('/services/<category>', methods=['GET'])
def services_by_category(category):
    branches = get_branches()
    result = dict()
    for b in branches:
        if category in b.categories:
            for c in b.categories[category]:
                result[c] = service[c]
    print(json.dumps(result, indent=2))
    return jsonify(result)


if __name__ == '__main__':
    app.run(host='0.0.0.0')