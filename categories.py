category = {
  "additional_features": "Дополнительные услуги",
  "currency_deposit": "Текущий депозит",
  "loans_private_individuals": "Налоги",
  "issued_bank_cards": "Банковская карта",
  "remote_service": "Удаленные услуги"
}

service = {
  "pay_for_internet_access": "Оплата интернета",
  "payment_for_communal_services": "ЖКХ",
  "payment_of_commercial_tv": "Телевидение",
  "payment_of_ip_telephony": "IP-телефония",
  "payment_for_phone": "Мобильная связь",
  "payment_of_cellular_communication": "Сотовая связь",
  "payment_of_fines_stsi": "Штрафы ГАИ",
  "payment_for_education": "Образовательные",
  "payments_to_charities": "Благотворительность",
  "payment_security_system": "Охрана",
  "deposit_rur": "Депозит RUR",
  "deposit_usd": "Депозит USD",
  "deposit_eur": "Депозит EUR",
  "autocredit": "Платежи по автокредиту",
  "mortgage": "Ипотечные платежи",
  "consumer_credit": "Потребительский кредит",
  "target_credit": "Целевой кредит",
  "visa_international": "Оформить Visa",
  "eurocard_mastercard": "Оформить Mastercard",
  "americanexpress": "Оформить AmericanExpress",
  "by_internet": "Интернет-услуги"
}
