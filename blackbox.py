from datetime import datetime, timedelta
import numpy as np
import geopy.distance
import requests
import re
import time
import pandas as pd
from sklearn.ensemble import RandomForestClassifier

from parse_xml import get_branches


def return_dist_minute_score(point1, point2):
    # text
    url = 'https://www.google.com/maps/dir/'
    url += str(point1[0]) + ',' + str(point1[1]) + '/'
    url += str(point2[0]) + ',' + str(point2[1])
    text = requests.get(url).text

    # km
    km = re.findall(r'\d+,\d+\xa0км', text)
    if len(km) == 0:
        km = 0
    else:
        km = float(km[0].split("""\xa0""")[0].replace(',', '.'))

    if km == 0:
        m = re.findall(r'\d+\xa0м', text)
        if len(m) == 0:
            m = 0
        else:
            m = float(m[0].split("""\xa0""")[0])
    else:
        m = 0

    hours = re.findall(r'\d+ ч.', text)
    if len(hours) == 0:
        hours = 0
    else:
        hours = float(hours[0].split(' ')[0])

    minutes = float(re.findall(r'\d+ мин', text)[0].split(' ')[0])

    dist = (km * 1000 + m) / 1000
    score = dist / minutes * 60
    if score < 10:
        score = 'Пешком'
    else:
        score = 'На машине'
    return dist, minutes, score


def nearest_time(minutes_from_now):
    now_time = datetime.now()
    from_time = now_time + timedelta(minutes=minutes_from_now)
    return from_time + timedelta(minutes=int(np.random.uniform(20, 40)))


def calc_dist(point1, point2):
    return geopy.distance.vincenty(point1, point2).km


def calc_path_time(point1, point2):
    return geopy.distance.vincenty(point1, point2).km * 10


def calculate_score(t_path, t_wait, alpha=0.5):
    return (1 + alpha) * t_path + t_wait


def make_order(list_of_branches):
    data = pd.read_csv('./train.txt', sep='  ', header=None)
    X = data[[1, 2, 4, 5]]
    y = data[6].values
    rf_model = RandomForestClassifier(n_estimators=100)
    rf_model.fit(X, y)
    ar = []
    for i in range(3):
        current_ar = []
        current_ar.append(list_of_branches[i]['lat'])
        current_ar.append(list_of_branches[i]['lon'])
        current_ar.append(list_of_branches[i]['dist'])
        current_ar.append(list_of_branches[i]['path_time'])
        ar.append(current_ar)
    current_ar = np.array(ar)
    predicts = rf_model.predict_proba(current_ar)
    current_result = [(list_of_branches[i], predicts[i][0]) for i in range(3)]
    current_result = sorted(current_result, key=lambda x: x[1], reverse=True)
    current_result = [current_result[i][0] for i in range(3)]
    return current_result


class Client(object):
    def __init__(self, user_id, service, coords):
        self.user_id = user_id
        self.coords = coords
        self.service = service

        self.chosen_branch_id = ""


class BlackBox(object):
    def __init__(self):
        self.branches = get_branches()
        self.waiting_clients = dict()

    def nearest_branches(self, user_id, service_id, user_coords):
        now_time = datetime.now()
        client = Client(user_id, service_id, user_coords)
        path_time = [calc_path_time((x.lat, x.lon), client.coords) for x in self.branches]
        # path_time = [return_dist_minute_score((x.lat, x.lon), client.coords)[1] for x in self.branches]
        dists = [calc_dist((x.lat, x.lon), client.coords) for x in self.branches]
        # dists = [return_dist_minute_score((x.lat, x.lon)[0], client.coords) for x in self.branches]
        # path_type = [return_dist_minute_score((x.lat, x.lon)[2], client.coords) for x in self.branches]
        nearest_clear_slots = [nearest_time(x) for x in path_time]
        wait_time = [(nearest_clear_slots[i] - now_time).total_seconds() // 60 + 1 - path_time[i] \
                     for i in range(len(path_time))]
        scores_25 = [calculate_score(path_time[i], wait_time[i], 0.25) for i in range(len(path_time))]

        top_indexes = np.argsort(scores_25)[:3]#[::-1]
        top_3_branches = np.array(self.branches)[top_indexes]
        list_res = []
        for i in range(3):
            current_dict = dict()
            current_branch = top_3_branches[i]
            current_dict['address'] = current_branch.address
            current_dict['lat'] = current_branch.lat
            current_dict['lon'] = current_branch.lon
            try:
                time.sleep(1)
                ss_current = return_dist_minute_score((current_branch.lat, current_branch.lon), client.coords)
                current_dict['path_type'] = ss_current[2]
                current_dict['dist'] = ss_current[0]
                current_dict['path_time'] = str(int(ss_current[1]))
            except:
                current_dict['path_type'] = 'На машине'
                current_dict['dist'] = dists[top_indexes[i]]
                current_dict['path_time'] = str(int(path_time[top_indexes[i]]))
            current_dict['path_link'] = 'https://www.google.com/maps/dir/'
            current_dict['path_link'] += str(client.coords[0]) + ',' + str(client.coords[1]) + '/'
            current_dict['path_link'] += str(current_branch.lat) + ',' + str(current_branch.lon)
            hour = nearest_clear_slots[top_indexes[i]].hour
            minute = nearest_clear_slots[top_indexes[i]].minute
            current_dict['visit_time'] = str(hour) + ':' + str(minute)
            list_res.append(current_dict)
        list_res = make_order(list_res)
        # top_3_branches = [(x, client) for x in top_3_branches]
        self.waiting_clients[client.user_id] = list_res
        return list_res

    def info_about_chosen_branch(self, user_id, chosen_branch_index):
        result = self.waiting_clients[user_id][chosen_branch_index]
        with open('./train.txt', 'a') as f:
            current_text = ""
            current_text += user_id + '  '
            current_text += str(self.waiting_clients[user_id][chosen_branch_index]['lat']) + '  '
            current_text += str(self.waiting_clients[user_id][chosen_branch_index]['lon']) + '  '
            current_text += str(self.waiting_clients[user_id][chosen_branch_index]['path_type']) + '  '
            current_text += str(self.waiting_clients[user_id][chosen_branch_index]['dist']) + '  '
            current_text += str(self.waiting_clients[user_id][chosen_branch_index]['path_time']) + '  '
            current_text += str(chosen_branch_index)
            f.write(current_text + '\n')
        self.waiting_clients.pop(user_id)
        return result
