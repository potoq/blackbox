import json
import logging
import os
import shutil
import xml.etree.ElementTree as ET

import requests


logging.getLogger('').setLevel(logging.NOTSET)


class Branch(object):
    def __init__(self):
        self.id = ''
        self.services = set()
        self.lat = 0.0
        self.lon = 0.0
        self.name = ''
        self.address = ''
        self.working_time = ''
        self.info_page = ''
        self.categories = {}


def remote_get_branches():
    url = 'http://77.244.213.126:10010/v1/b'
    response = requests.get(url)
    result = []
    for id in response.json()['branches']:
        branch = Branch()
        response = requests.get('http://77.244.213.126:10010/v1/b/' + id)
        b = json.loads(response.json()['result'])
        branch.__dict__.update(b)
        for services in branch.categories.values():
            for service in services:
                branch.services.add(service)
        result.append(branch)
    return result


branches = []


def get_branches():
    global branches
    if len(branches) == 0:
        try:
            branches = remote_get_branches()
        except:
            print('not available')
        if not len(branches):
            branches = parse_branches()
    return branches


def parse_branches(only_moscow=True):
    tree = ET.parse('branches.xml')
    root = tree.getroot()
    result = []
    for item in root.findall('./company'):
        branch = Branch()
        flag = 1
        for child in item:
            if child.tag == 'company-id':
                branch.id = child.text
            elif child.tag == 'address':
                branch.address = child.text.strip()
            elif child.tag == 'info-page':
                branch.info_page = child.text
            elif child.tag == 'working-time':
                branch.working_time = child.text.strip()
            elif child.tag == 'coordinates':
                try:
                    # NOTE: lat long wrong from URAL SIB
                    branch.lat = float(child.find('lon').text)
                    branch.lon = float(child.find('lat').text)
                except:
                    flag = 0
            elif child.tag == 'feature-enum-multiple':
                branch.services.add(child.attrib['value'])
                values = branch.categories.get(child.attrib['name'], [])
                values.append(child.attrib['value'])
                branch.categories[child.attrib['name']] = values
        if flag:
            if only_moscow:
                if 'город Москва' in branch.address:
                    result.append(branch)
            else:
                result.append(branch)
    return result


def download_branches():

    import requests

    # url of rss feed
    url = 'https://www.uralsib.ru/raw/offices-xml/yandex'

    # creating HTTP response object from given url
    resp = requests.get(url)

    # saving the xml file
    with open('branches.xml', 'wb') as f:
        f.write(resp.content)


def commit_branches():
    branches = parse_branches()
    for b in branches:
        a = {
            'id' : b.id,
            'lat' : b.lat,
            'lon' : b.lon,
            'name': b.name,
            'address' : b.address,
            'info_page': b.info_page,
            'working_time' : b.working_time,
            "categories" : b.categories
        }
        response = requests.post('http://77.244.213.126:10010/v1/b', json=a)
        print(response.status_code)


def test_fetch():
    b = {
        'user_id': 'ffref',
        'service_id': "credits",
        'lat': 55.778961,
        'lon': 37.400621,
    }

    response = requests.post('http://10.20.3.7:5000/fetch', json=b)
    print(response.json())


def test_choose():
    b = {
        'user_id': 'ffref',
        'branch_id': 0
    }

    response = requests.post('http://10.20.3.7:5000/choose', json=b)
    print(response.json())


def test_image():
    b = {
        'text': 'ffrefgtrg1232421515tgtrgrtgrt',
    }

    r = requests.post('http://77.244.213.126:3333/', json=b, stream=True)
    r.raw.decode_content = True
    with open('file.png', 'wb') as f:
        r.raw.decode_content = True
        shutil.copyfileobj(r.raw, f)


# https://calendar.google.com/calendar/r/eventedit?text=%D0%97%D0%B0%D0%BF%D0%B8%D1%81%D1%8C+%D0%B2+%D0%B1%D0%B0%D0%BD%D0%BA+%D0%A3%D0%A0%D0%90%D0%9B%D0%A1%D0%98%D0%91&dates=20190428T100100Z%2F20190428T102000Z&location=%D0%B3%D0%BE%D1%80%D0%BE%D0%B4+%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0%2C+%D0%BF%D1%80%D0%BE%D1%81%D0%BF%D0%B5%D0%BA%D1%82+%D0%9C%D0%B0%D1%80%D1%88%D0%B0%D0%BB%D0%B0+%D0%96%D1%83%D0%BA%D0%BE%D0%B2%D0%B0%2C+%D0%B4%D0%BE%D0%BC+58%2C+%D0%BA%D0%BE%D1%80%D0%BF%D1%83%D1%81+1
if __name__ == "__main__":
    remote_get_branches()
    test_fetch()
    test_choose()
    #commit_branches()
    #test_image()